﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

	// Update is called once per frame
	void Update () {
        if (Gamemanager.instance.CanTakeTurn == true)
        {
            
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                Gamemanager.instance.Actions--;
                pawn.MoveVertical(1);
            }

            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                Gamemanager.instance.Actions--;
                pawn.MoveVertical(-1);
            }

            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                Gamemanager.instance.Actions--;
                pawn.MoveHorizontal(1);
            }

            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Gamemanager.instance.Actions--;
                pawn.MoveHorizontal(-1);
            }

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                Gamemanager.instance.Actions--;
                pawn.Shoot();
            }
        }
    }
}
