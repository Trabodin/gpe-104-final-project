﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    public Transform tf;
    public AudioSource As;

    void Start () {
        tf = GetComponent<Transform>();
        As = GetComponent<AudioSource>();
        Gamemanager.instance.Player.pawn = this;
	}

    public virtual void MoveVertical(int dir)
    {    
    }
    public virtual void MoveHorizontal(int dir)
    {
    }

    public virtual void Shoot()
    {
    }
}
