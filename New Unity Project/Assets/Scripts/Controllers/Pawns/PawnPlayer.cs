﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnPlayer : Pawn {

    public GameObject NormalBullet;

    public GameObject Turret;

    public GameObject DeathAnim;

    public float CheckDistance;
    // Update is called once per frame
    void Update () {

        Debug.DrawLine(tf.position, tf.position + new Vector3(0, -1 * CheckDistance, 0));
        Gamemanager.instance.TurretRotation = Turret.transform.rotation * Quaternion.Euler(0,0,-90);
    }

    //raycast to see if the spot is clear then moves the distance of a space in that direction
    public override void MoveVertical(int dir)
    {
        RaycastHit2D hitinfo = Physics2D.Raycast(tf.position, new Vector3(0, dir, 0),.64f);

        if (hitinfo.collider != null && hitinfo.collider.isTrigger == false)
        {
            Debug.Log(hitinfo.collider.gameObject.tag);

        }
        else if (hitinfo.collider == null || hitinfo.collider.isTrigger == true)
        {
            tf.position = tf.position + new Vector3(0, .64f * dir, 0);
            
        }
    }
    public override void MoveHorizontal(int dir)
    {
        RaycastHit2D hitinfo = Physics2D.Raycast(tf.position, new Vector3(dir, 0, 0), .64f);
     
        if (hitinfo.collider != null && hitinfo.collider.isTrigger == false)
        {
            Debug.Log(hitinfo.collider.gameObject.tag);

        }
        else if (hitinfo.collider == null || hitinfo.collider.isTrigger == true)
        {
            tf.position = tf.position + new Vector3(.64f * dir, 0, 0);
            
        }
    }

    //intantiates the bullet at the rotation of the turret
    public override void Shoot()
    {

        Instantiate(NormalBullet, tf.position, Turret.transform.rotation);
        As.Play();
    }
}
