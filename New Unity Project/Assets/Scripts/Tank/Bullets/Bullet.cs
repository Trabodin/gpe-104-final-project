﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour {
    public Transform tf;
    public Rigidbody2D rb;
   
    public float Speed;
    public int Bounces;

    public Vector3 dir;

    // Use this for initialization
    void Start()
    {

        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        

        dir =Gamemanager.instance.TurretRotation * Vector3.up * Speed;
        rb.AddForce(dir);

        // so you cant spam bulletsor move while shooting
        Gamemanager.instance.CanTakeTurn = false;
    }

    // Update is called once per frame
    void Update () {
        // if its out of bounces it dies and lets you continue with the game
		 if (Bounces <= 0)
         {
            Gamemanager.instance.CanTakeTurn = true;
            Destroy(gameObject);
         }
	}

   // if it somehow gets off the screen this will kill it
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "GameBoard")
        {
            Destroy(gameObject);
        }
    }
}
