﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAim : MonoBehaviour {
    public Transform tf;
	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 Dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - tf.position;
        float Angle = Mathf.Atan2(Dir.y, Dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(Angle, Vector3.forward);
        tf.rotation = rotation;
	}
}
