﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {

    public Sprite Available;
    public Sprite Blocked;

    public Button button;

    public int levelNum;

	// Use this for initialization
	void Start () {
		if (Gamemanager.instance.Passed[levelNum - 1] == true)
        {
            button.image.sprite = Available;
        }
        else if (Gamemanager.instance.Passed[levelNum - 1] == false)
        {
            button.interactable = false;
            button.image.sprite = Blocked;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
