﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Gamemanager : MonoBehaviour {

    public static Gamemanager instance;

    public PlayerController Player;

    public INGameMEnu menu;

    public Quaternion TurretRotation;

    public bool CanTakeTurn = false;

   

    [Header("Game Info")]
    public int Actions;
    public List<bool> Passed;

	// Use this for initialization
	void Awake () {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            GetComponent<PlayerController>().enabled = false;
        }
        else
        {
            Destroy(this);
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		if (Actions <= 0 && CanTakeTurn ==true)
        {
            OnLoss();
        }
        
	}

    //Functions And Buttons Section 

    public void OnWin()
    {
        // makes it so you cant move after the menu pops up
        CanTakeTurn = false;
        menu.Win();
    }

    public void OnLoss()
    {
        //checks to see if you have already one so you dont get 2 things popping up saying you won and lost
        if (menu.won != true)
        {
            CanTakeTurn = false;
            menu.Loss();
        } 
    }
}
