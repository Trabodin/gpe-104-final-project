﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    public GameObject InitalMenu;
    public GameObject LevelSelect;


    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void QuitGame()
    {
        Application.Quit();
    }
    
    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
        Gamemanager.instance.gameObject.GetComponent<PlayerController>().enabled = true;
        Gamemanager.instance.CanTakeTurn = true;
    }

    //set one game object holding the menue active and the otherone inactive
    public void LevelMenuEnter()
    {    
        InitalMenu.SetActive(false);
        LevelSelect.SetActive(true);
    }
    public void LevelMenuExit()
    {
        InitalMenu.SetActive(true);
        LevelSelect.SetActive(false);
    }

}
