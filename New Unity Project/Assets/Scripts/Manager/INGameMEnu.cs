﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INGameMEnu : MonoBehaviour {

    public GameObject InGameMenu;
    public GameObject WinScreen;
    public GameObject LossScreen;

    public int levelNum;
    public bool won;

    // Use this for initialization
    void Start()
    {
        Gamemanager.instance.menu = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadLevel(string level)
    {
        Gamemanager.instance.gameObject.GetComponent<PlayerController>().enabled = false;
        Gamemanager.instance.Actions = 10;
        Gamemanager.instance.CanTakeTurn = false;
        SceneManager.LoadScene(level);
    }
    public void Win()
    {
        //sets the win screen active and also unlocks the next level to play
        InGameMenu.SetActive(false);
        WinScreen.SetActive(true);
        if (levelNum < Gamemanager.instance.Passed.Count)
        {
            Gamemanager.instance.Passed[levelNum] = true;
            won = true;
        }
        
    }
    public void Loss()
    {
        //activates the loss screen and deactivates the in game overlay
        InGameMenu.SetActive(false);
        LossScreen.SetActive(true);
    }
  
}
