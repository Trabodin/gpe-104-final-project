﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
    public float Length;

    public AudioClip clip;

	// Use this for initialization
	void Start () {
        AudioSource.PlayClipAtPoint(clip, transform.position);
        Destroy(gameObject, Length);
	}

}
