﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodenBox : Brick {

    public AudioClip clip;

    public override void OnHit()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
        Destroy(gameObject);
    }
    
}
