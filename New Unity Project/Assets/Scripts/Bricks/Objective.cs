﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : Brick {
    public GameObject Explosion;

    public override void OnHit()
    {
        // if it gets hit you win the level
        Gamemanager.instance.OnWin();
        Instantiate(Explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
