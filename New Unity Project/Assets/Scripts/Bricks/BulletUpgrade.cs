﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletUpgrade : Brick {

    public GameObject UpgradedBullet;
    public AudioClip clip;

    public override void OnHit()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
        Gamemanager.instance.Player.pawn.gameObject.GetComponent<PawnPlayer>().NormalBullet = UpgradedBullet;
        Destroy(gameObject);
    }
}
