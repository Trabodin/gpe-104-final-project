﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalBox : Brick {

    public SpriteRenderer sr;

    public int health;

    public Color hurtColor;

    public void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public override void OnHit()
    {
        if (health >= 1)
        {
            health--;
            if (health == 0)
            {
                Destroy(gameObject);
            }
        }

        if (health == 1)
        {
            sr.color = hurtColor;
        }
    }

}
