﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBarrel : Brick{
    public GameObject Explosion;

    public override void OnHit()
    {
        RaycastHit2D hitinfoR = Physics2D.Raycast(transform.position, new Vector3(1, 0, 0), .64f);

        Instantiate(Explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);

        if (hitinfoR.collider != null)
        {
            if(hitinfoR.collider.gameObject.tag == "Brick")
            {
                hitinfoR.collider.gameObject.GetComponent<Brick>().OnHit();
            }
        }
        RaycastHit2D hitinfoL  = Physics2D.Raycast(transform.position, new Vector3(-1, 0, 0), .64f);
        
        if (hitinfoL.collider != null)
        {
            if (hitinfoL.collider.gameObject.tag == "Brick")
            {
                hitinfoL.collider.gameObject.GetComponent<Brick>().OnHit();
            }
        }
        RaycastHit2D hitinfoU = Physics2D.Raycast(transform.position,  new Vector3(0, 1, 0), .64f);
        
        if (hitinfoU.collider != null)
        {
            if (hitinfoU.collider.gameObject.tag == "Brick")
            {
                hitinfoU.collider.gameObject.GetComponent<Brick>().OnHit();
            }
        }
        RaycastHit2D hitinfoD = Physics2D.Raycast(transform.position, new Vector3(0, -1, 0), .64f);
        
        if (hitinfoD.collider != null)
        {
            if (hitinfoD.collider.gameObject.tag == "Brick")
            {
                hitinfoD.collider.gameObject.GetComponent<Brick>().OnHit();
            }
        }
        
      
    }
}
