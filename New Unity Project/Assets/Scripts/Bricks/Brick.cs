﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {
    public bool MoveAble;
    public bool DestroyAble;

    public virtual void OnHit()
    {

    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            OnHit();
        }
    }
   
}
