﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceBrick : Brick {
    public AudioSource As;
    public GameObject bullet;

    void Start()
    {
        As = GetComponent<AudioSource>();
    }

    public override void OnHit()
    {
        As.Play();

        // adds an extra bounce to the bullet that hit it
        Bullet script = bullet.GetComponent<Bullet>();
        script.Bounces++;
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            bullet = other.gameObject;
            OnHit();
        }
    }
}
